module gitlab.com/david_mbuvi/go_asterisks

go 1.18

require golang.org/x/crypto v0.2.0

require (
	golang.org/x/sys v0.2.0 // indirect
	golang.org/x/term v0.2.0 // indirect
)
